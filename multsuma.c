#include <stdio.h>

int main ()
{
    int num;
    int mult;
    int resultado;
    int i = 0;
    
    printf ("Introduce un numero y un multiplicador \n");
    scanf ("%i %i", &num, &mult);
    getchar();
    printf ("%i * %i = ", num, mult);
    resultado = num * mult;
    
    if (mult>0)
    {
        while (i<mult - 1)
        {
            printf ("%i + ", num);
            i++;
        }
    }
    else if (0>mult)
    {
        while (i>mult + 1)
        {
            printf ("-%i ", num);
            i--;
        }
        
    }
    
    if (mult == 0)
    {
        printf ("%i \n", resultado);
    }
    else
    {
        if (mult<0) 
        {
            printf ("-");
        }
        
        printf ("%i = %i \n", num, resultado);
    }
    
    system ("pause");
    getchar();
    
    return 0;

}