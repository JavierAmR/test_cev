#include <stdio.h>

int main ()
{
    int num1;
    int numre;
    
    printf ("Introduce un numero para ver su tabla de multiplicar: \n");
    scanf ("%i %i", &num1, &numre);
    
    for (int i = 0; i<numre + 1; i++)
    {
        
        printf ("%i * %i = %i \n", num1, i, num1 * i);
    }
    system ("pause");
    getchar ();
    return 0;
}